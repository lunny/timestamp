# Golang package to operate time as timestamp

[![Build Status](https://drone.gitea.com/api/badges/lunny/timestamp/status.svg)](https://drone.gitea.com/lunny/timestamp) [![](http://gocover.io/_badge/gitea.com/lunny/timestamp)](http://gocover.io/gitea.com/lunny/timestamp)
[![](https://goreportcard.com/badge/gitea.com/lunny/timestamp)](https://goreportcard.com/report/gitea.com/lunny/timestamp)

## Installation

```
go get gitea.com/lunny/timestamp
```

## Usage

```Go
now := timestamp.TimeStampNow()
fmt.Println("Current is", now.Format("2006-01-02 15:04:05"))
```